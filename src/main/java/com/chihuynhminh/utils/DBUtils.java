package com.chihuynhminh.utils;

import com.chihuynhminh.Client.OASTClient;
import com.chihuynhminh.Client.OASTPayload;
import com.chihuynhminh.OASTFixed;
import com.chihuynhminh.Obj.OASTObj;
import org.json.JSONObject;

import javax.swing.table.AbstractTableModel;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DBUtils {
    static String log_file = "logs.bin";
    static String server_file = "servers.bin";
    static String payload_file = "payloads.bin";
    static String ipinfo_file = "ipinfos.bin";
    static String config_file_path = Paths.get(OASTFixed.default_config_folder, "config.json").toString();
    static String oldversion = "1.0";

    static public void loadConfig() {
        try {
            File f = new File(OASTFixed.default_config_folder);

            if (!f.exists()) {
                boolean success = f.mkdir();
                if (!success) {
                    OASTFixed._api.logging().logToOutput("Cannot create folder .OASTFixed");
                    return;
                }
            }

            f = new File(config_file_path);
            if (!f.exists()) {
                    OASTFixed.oastFixedTab.logsFolderTextField.setText(OASTFixed.default_config_folder);
            } else {
                String config = new String(Files.readAllBytes(Paths.get(config_file_path)));
                JSONObject config_json = new JSONObject(config);
                String log_folder = config_json.getString("log_folder");
                if (config_json.has("version"))
                    oldversion = config_json.getString("version");
                if (config_json.has("ipinfo_token"))
                    OASTFixed.oastFixedTab.ipInfoTKField.setText(config_json.getString("ipinfo_token"));
                OASTFixed.oastFixedTab.logsFolderTextField.setText(log_folder);
            }

            // Load DB
            DBUtils.loadServers();
            DBUtils.loadPayloads();
            DBUtils.loadLogs();
            DBUtils.loadIpInfo();

            // Update version in config
            saveConfig();
        } catch (Exception ex) {
            ex.printStackTrace(OASTFixed._api.logging().error());
        }
    }

    static public void saveConfig() {
        try {
            JSONObject config_json = new JSONObject();
            config_json.put("log_folder", OASTFixed.oastFixedTab.logsFolderTextField.getText());
            config_json.put("version", OASTFixed.extension_version);
            config_json.put("ipinfo_token", new String(OASTFixed.oastFixedTab.ipInfoTKField.getPassword()));

            FileWriter fileWriter = new FileWriter(config_file_path);
            fileWriter.write(config_json.toString());
            fileWriter.close();
        } catch (Exception ex) {
            ex.printStackTrace(OASTFixed._api.logging().error());
        }
    }

    static public void saveIpInfo() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(Paths.get(OASTFixed.oastFixedTab.logsFolderTextField.getText(), ipinfo_file).toString()));
            objectOutputStream.writeObject(OASTFixed.oastFixedTab.ipInfoMap);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace(OASTFixed._api.logging().error());
        }
    }

    static public void saveLogs() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(Paths.get(OASTFixed.oastFixedTab.logsFolderTextField.getText(), log_file).toString()));
            objectOutputStream.writeObject(OASTFixed.oastFixedTab.oastList);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace(OASTFixed._api.logging().error());
        }
    }

    static public void saveServers() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(Paths.get(OASTFixed.oastFixedTab.logsFolderTextField.getText(),  server_file).toString()));
            objectOutputStream.writeObject(OASTFixed.oastFixedTab.oastClients);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace(OASTFixed._api.logging().error());
        }
    }

    static public void savePayloads() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(Paths.get(OASTFixed.oastFixedTab.logsFolderTextField.getText(),  payload_file).toString()));
            objectOutputStream.writeObject(OASTFixed.oastFixedTab.oastPayloads);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace(OASTFixed._api.logging().error());
        }
    }

    static public void loadIpInfo() {
        String filename = Paths.get(OASTFixed.oastFixedTab.logsFolderTextField.getText(),  ipinfo_file).toString();
        File f = new File(filename);
        if (f.exists()) {
            try (FileInputStream fis = new FileInputStream(filename);
                 ObjectInputStream ois = new ObjectInputStream(fis)) {
                OASTFixed.oastFixedTab.ipInfoMap = (HashMap<String, String>) ois.readObject();
                return;
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace(OASTFixed._api.logging().error());
            }
        }
        OASTFixed.oastFixedTab.ipInfoMap = new HashMap<String, String>();
    }

    static public void loadLogs() {
        String filename = Paths.get(OASTFixed.oastFixedTab.logsFolderTextField.getText(), log_file).toString();
        File f = new File(filename);
        if (f.exists()) {
            List<OASTObj> list = new ArrayList<>();
            try (FileInputStream fis = new FileInputStream(filename);
                 ObjectInputStream ois = new ObjectInputStream(fis)) {
                list = (List<OASTObj>) ois.readObject();
                if (Double.parseDouble(oldversion) < 2.3) {
                    for (int i = 0; i < list.size(); ++i) {
                        try {
                            list.get(i)._color = null;
                        } catch (Exception ex) {
                            ex.printStackTrace(OASTFixed._api.logging().error());
                        }
                    }
                }

                OASTFixed.oastFixedTab.oastList.addAll(list);
                OASTFixed.oastFixedTab.oastTableModel.fireTableDataChanged();
                if (Double.parseDouble(oldversion) < 2.3)
                    saveLogs();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    static public void loadServers() {
        String filename = Paths.get(OASTFixed.oastFixedTab.logsFolderTextField.getText(),  server_file).toString();
        File f = new File(filename);
        if (f.exists()) {
            List<OASTClient> list = new ArrayList<>();

            try (FileInputStream fis = new FileInputStream(filename);
                 ObjectInputStream ois = new ObjectInputStream(fis)) {
                list = (List<OASTClient>) ois.readObject();

                if (oldversion.equals("1.0")) {
                    for (OASTClient client : list) {
                        client._isEnable = true;
                    }
                }

                OASTFixed.oastFixedTab.oastClients.addAll(list);

                ((AbstractTableModel) OASTFixed.oastFixedTab.serverTable.getModel()).fireTableRowsInserted(0, list.size());

                if (oldversion.equals("1.0"))
                    saveServers();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    static public void loadPayloads() {
        String filename = Paths.get(OASTFixed.oastFixedTab.logsFolderTextField.getText(),  payload_file).toString();
        File f = new File(filename);
        if (f.exists()) {
            List<OASTPayload> list = new ArrayList<>();
            try (FileInputStream fis = new FileInputStream(filename);
                 ObjectInputStream ois = new ObjectInputStream(fis)) {
                list = (List<OASTPayload>) ois.readObject();

                // Remove all old payload with server location
                if (oldversion.equals("1.0")) {
                    for (OASTPayload pl : list) {
                        OASTClient client = OASTFixed.oastFixedTab.oastClients.stream().filter(cl -> cl._id == pl._serverId).findFirst().get();
                        if (pl._payload.contains(client._serverLocation)) {
                            pl._payload = pl._payload.replaceAll("." + client._serverLocation, "");
                        }
                    }
                }

                OASTFixed.oastFixedTab.oastPayloads.addAll(list);

                if (oldversion.equals("1.0"))
                    savePayloads();

                ((AbstractTableModel) OASTFixed.oastFixedTab.payloadTable.getModel()).fireTableRowsInserted(0, list.size());
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
