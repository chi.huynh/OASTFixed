package com.chihuynhminh.utils;

import com.chihuynhminh.OASTFixed;

import java.util.HashMap;
import java.util.Objects;

public class DNSUtils {
    static public HashMap<Integer, String> buildQTYPEMap() {
        // Ref: https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml
        HashMap<Integer, String> qtype = new HashMap<Integer, String>();
        qtype.put(1, "A");
        qtype.put(2, "NS");
        qtype.put(3, "MD");
        qtype.put(4, "MF");
        qtype.put(5, "CNAME");
        qtype.put(6, "SOA");
        qtype.put(7, "MB");
        qtype.put(8, "MG");
        qtype.put(9, "MR");
        qtype.put(10, "NULL");
        qtype.put(11, "WKS");
        qtype.put(12, "PTR");
        qtype.put(13, "HINFO");
        qtype.put(14, "MINFO");
        qtype.put(15, "MX");
        qtype.put(16, "TXT");
        qtype.put(17, "RP");
        qtype.put(18, "AFSDB");
        qtype.put(19, "X25");
        qtype.put(20, "ISDN");
        qtype.put(21, "RT");
        qtype.put(22, "NSAP");
        qtype.put(23, "NSAP-PTR");
        qtype.put(24, "SIG");
        qtype.put(25, "KEY");
        qtype.put(26, "PX");
        qtype.put(27, "GPOS");
        qtype.put(28, "AAAA");
        qtype.put(29, "LOC");
        qtype.put(30, "NXT");
        qtype.put(31, "EID");
        qtype.put(32, "NIMLOC");
        qtype.put(33, "SRV");
        qtype.put(34, "ATMA");
        qtype.put(35, "NAPTR");
        qtype.put(36, "KX");
        qtype.put(37, "CERT");
        qtype.put(38, "A6");
        qtype.put(39, "DNAME");
        qtype.put(40, "SINK");
        qtype.put(41, "OPT");
        qtype.put(42, "APL");
        qtype.put(43, "DS");
        qtype.put(44, "SSHFP");
        qtype.put(45, "IPSECKEY");
        qtype.put(46, "RRSIG");
        qtype.put(47, "NSEC");
        qtype.put(48, "DNSKEY");
        qtype.put(49, "DHCID");
        qtype.put(50, "NSEC3");
        qtype.put(51, "NSEC3PARAM");
        qtype.put(52, "TLSA");
        qtype.put(53, "SMIMEA");
        qtype.put(54, "Unassigned");
        qtype.put(55, "HIP");
        qtype.put(56, "NINFO");
        qtype.put(57, "RKEY");
        qtype.put(58, "TALINK");
        qtype.put(59, "CDS");
        qtype.put(60, "CDNSKEY");
        qtype.put(61, "OPENPGPKEY");
        qtype.put(62, "CSYNC");
        qtype.put(63, "ZONEMD");
        qtype.put(64, "SVCB");
        qtype.put(65, "HTTPS");
        qtype.put(99, "SPF");
        qtype.put(100, "UINFO");
        qtype.put(101, "UID");
        qtype.put(102, "GID");
        qtype.put(103, "UNSPEC");
        qtype.put(104, "NID");
        qtype.put(105, "L32");
        qtype.put(106, "L64");
        qtype.put(107, "LP");
        qtype.put(108, "EUI48");
        qtype.put(109, "EUI64");
        qtype.put(249, "TKEY");
        qtype.put(250, "TSIG");
        qtype.put(251, "IXFR");
        qtype.put(252, "AXFR");
        qtype.put(253, "MAILB");
        qtype.put(254, "MAILA");
        qtype.put(256, "URI");
        qtype.put(257, "CAA");
        qtype.put(258, "AVC");
        qtype.put(259, "DOA");
        qtype.put(260, "AMTRELAY");
        qtype.put(32768, "TA");
        qtype.put(32769, "DLV");

        return qtype;
    }

    static public String getQtype(int code) {
        String type = OASTFixed.qtype.get(code);
        if (Objects.isNull(type))
            return "UNKNOWN";
        return type;
    }
}
