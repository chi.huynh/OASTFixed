package com.chihuynhminh.utils;

import com.chihuynhminh.OASTFixed;

public class IpInfoUtils {
    public static String getIpFromIpAndPort(String ip_port) {
        int colonIndex = ip_port.indexOf(":");
        if (colonIndex > -1) {
            return ip_port.substring(0, colonIndex);
        }
        return ip_port;
    }

    public static String getIpInfo(String ip, String token) {
        String url = "https://ipinfo.io/" + ip + "/json";
        if (!token.isEmpty())
            url += "?token=" + token;

        String ipinfo;

        try {
            ipinfo = HTTPUtils.sendRequest(url, "GET", new String[][]{}, "");
            if (ipinfo.contains("\"error\":")) {
                OASTFixed._api.logging().logToError("Error when get ipinfo " + ip + "\n" + ipinfo + "\n\n");
                return "";
            }
        } catch (Exception ex) {
            ex.printStackTrace(OASTFixed._api.logging().error());
            return "";
        }

        return ipinfo;
    }
}
