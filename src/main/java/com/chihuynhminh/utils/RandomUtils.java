package com.chihuynhminh.utils;

import com.chihuynhminh.OASTFixed;

import java.util.Base64;

public class RandomUtils {
    static public String genRandomStringFromTime(int len) {
        String s1 = Base64.getEncoder().encodeToString(
                Long.toString(System.currentTimeMillis()).getBytes());
        String s2 = Base64.getEncoder().encodeToString(
                Long.toString(System.currentTimeMillis()).getBytes());
        String s3 = (s1 + s2).toLowerCase().replaceAll(OASTFixed.validDomainNamePatten, "");
        String s = s3.substring(s3.length() - len, s3.length());
        return s;
    }
}
