package com.chihuynhminh.utils;

import com.chihuynhminh.OASTFixed;

import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.Map;

public class HTTPUtils {
    static public boolean isdebug = false;

    public static String sendRequest(String url, String method, String [][] headers, String requestBody) throws IOException {
        URL obj = new URL(url);

        HttpURLConnection con;
        if (isdebug) {
            trustAllHosts();
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1", 8080));
            con = (HttpURLConnection) obj.openConnection(proxy);
        }
        else
            con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod(method);

        for (String[] header : headers) {
            if (header.length == 2) {
                con.setRequestProperty(header[0], header[1]);
            }
        }

        if (!method.equals("GET")) {
            byte[] postData = requestBody.getBytes(StandardCharsets.UTF_8);
            int postDataLength = postData.length;
            con.setDoOutput(true);
            con.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(postData);
            }
        }

        int responseCode = con.getResponseCode();

        /*
        if (isdebug) {
            System.out.println("Response Code : " + responseCode);

            Map<String, List<String>> res_headers = con.getHeaderFields();
            for (String key : res_headers.keySet()) {
                System.out.println(key + " : " + res_headers.get(key));
            }
        }
        */

        // Extract body
        InputStream inputStream;
        if (responseCode > 399) {
            inputStream = con.getErrorStream();
        } else {
            inputStream = con.getInputStream();
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        String inputLine;
        StringBuilder responseBody = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            responseBody.append(inputLine);
        }
        in.close();

        /*
        if (isdebug)
            System.out.println("Response Body: " + responseBody.toString());
         */

        return responseBody.toString();
    }

    static public void trustAllHosts()
    {
        try
        {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509ExtendedTrustManager()
                    {
                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers()
                        {
                            return null;
                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
                        {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
                        {
                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] xcs, String string, Socket socket) throws CertificateException
                        {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] xcs, String string, Socket socket) throws CertificateException
                        {

                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException
                        {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException
                        {

                        }

                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new  HostnameVerifier()
            {
                @Override
                public boolean verify(String hostname, SSLSession session)
                {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        }
        catch (Exception e)
        {

        }
    }
}
