package com.chihuynhminh.ui;

import com.chihuynhminh.Client.BClient;
import com.chihuynhminh.Client.OASTClient;
import com.chihuynhminh.OASTFixed;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class PayloadTableModel extends AbstractTableModel {
    static String[] columnName = {"#", "Name", "OAST Server", "Payload" };
    static int[] columnWidth = { 10, 200, 200, 200 };

    @Override
    public int getRowCount() {
        return OASTFixed.oastFixedTab.oastPayloads.size();
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return OASTFixed.oastFixedTab.oastPayloads.get(rowIndex)._id + 1;
            case 1:
                return OASTFixed.oastFixedTab.oastPayloads.get(rowIndex)._name;
            case 2:
                int idServer = OASTFixed.oastFixedTab.oastPayloads.get(rowIndex)._serverId;
                OASTClient client = OASTFixed.oastFixedTab.oastClients.stream().filter(v -> v._id == idServer).findFirst().get();
                return (client._id + 1) + " - " + client._serverLocation;
            default:
                return OASTFixed.oastFixedTab.oastPayloads.get(rowIndex)._payload;
        }
    }

    @Override
    public String getColumnName(int columnIndex){
        if (columnIndex < columnName.length) {
            return columnName[columnIndex];
        }
        return "";
    }

    static public void setColumnWidth(JTable table){
        for (int i = 0; i < columnWidth.length; ++i) {
            table.getColumnModel().getColumn(i).setPreferredWidth(columnWidth[i]);
        }
    }
}
