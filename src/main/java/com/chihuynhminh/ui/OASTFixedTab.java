package com.chihuynhminh.ui;

import burp.api.montoya.core.ByteArray;
import burp.api.montoya.http.message.requests.HttpRequest;
import burp.api.montoya.http.message.responses.HttpResponse;
import burp.api.montoya.ui.Theme;
import burp.api.montoya.ui.editor.EditorOptions;
import burp.api.montoya.ui.editor.HttpRequestEditor;
import burp.api.montoya.ui.editor.HttpResponseEditor;
import burp.api.montoya.ui.editor.RawEditor;
import com.chihuynhminh.Client.BClient;
import com.chihuynhminh.Client.IClient;
import com.chihuynhminh.Client.OASTClient;
import com.chihuynhminh.Client.OASTPayload;
import com.chihuynhminh.OASTFixed;
import com.chihuynhminh.Obj.*;
import com.chihuynhminh.utils.DBUtils;
import com.chihuynhminh.utils.IpInfoUtils;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class OASTFixedTab {
    public JPanel mainPanel;
    private JTabbedPane detailTabbedPane;
    private JButton pollButton;
    public JTable oastTable;

    public JTable serverTable;
    private JButton addServerButton;
    private JButton editServerButton;
    private JButton removeServerButton;
    public JTable payloadTable;
    private JButton addPayloadButton;
    private JButton editPayloadButton;
    private JButton copyPayloadButton;
    private JButton removePayloadButton;
    private JLabel oastServerListDetailLabel;
    private JTextPane descriptionTextPane;
    public JTextField logsFolderTextField;
    private JButton changeFolderButton;
    private JSpinner autoPollSpinner;
    private JTextField logSearchTextField;
    public JComboBox colorCombobox;
    private JComboBox protocolCombobox;
    private JButton queryAllIPButton;
    private JButton changeIPTokenButton;
    public JPasswordField ipInfoTKField;

    // Detail tabs
    HttpRequestEditor requestEditor;
    HttpResponseEditor responseEditor;
    RawEditor rawRequestEditor;
    RawEditor ipInfoEditor;

    public static final String[] colors = { null, "red", "orange", "yellow", "green", "cyan", "blue", "pink", "magenta", "gray" };

    public List<OASTClient> oastClients;
    public List<OASTPayload> oastPayloads;
    public List<OASTObj> oastList;
    public HashMap<String, String> ipInfoMap;

    public final OASTTableModel oastTableModel = new OASTTableModel();

    ChangeListener detailTabbedPaneChangeListener = new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
            detailTabbedPaneSelectionChange();
        }
    };

    public Thread autoPollThread = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep((int)autoPollSpinner.getValue() * 1000);

                    // If extension unloaded, stop polling
                    JTabbedPane rootTabbedPane = (JTabbedPane) OASTFixed.oastFixedTab.mainPanel.getParent();
                    if (Objects.isNull(rootTabbedPane))
                        return;

                    poll();
                } catch (Exception e) {
                    e.printStackTrace(OASTFixed._api.logging().error());
                }
            }
        }
    });

    static boolean isAddRootPaneChangeEvent = false;

    public OASTFixedTab() {
        // Init list data
        oastClients = new ArrayList<>();
        oastPayloads = new ArrayList<>();
        oastList = new ArrayList<>();

        // Init editor components
        detailTabbedPane.addChangeListener(detailTabbedPaneChangeListener);
        requestEditor = OASTFixed._api.userInterface().createHttpRequestEditor(EditorOptions.READ_ONLY);
        responseEditor = OASTFixed._api.userInterface().createHttpResponseEditor(EditorOptions.READ_ONLY);
        rawRequestEditor = OASTFixed._api.userInterface().createRawEditor(EditorOptions.READ_ONLY);
        ipInfoEditor = OASTFixed._api.userInterface().createRawEditor(EditorOptions.READ_ONLY);

        // Click event poll button
        pollButton.addActionListener(e -> poll());

        /*******************
         * Config set setup
         *******************/
        queryAllIPButton.addActionListener(l -> queryAllIpInfo());
        changeIPTokenButton.addActionListener(l -> changeIPToken());

        /*******************
         * Filter set setup
         *******************/
        // Search box
        logSearchTextField.addActionListener(l -> updateFilter());

        // Color combobox
        try {
            colorCombobox.addItem(0);
            for (String color : colors) colorCombobox.addItem(color);
            colorCombobox.setRenderer(new ComboboxColorRenderer());
        } catch (Exception ex) {
            ex.printStackTrace(OASTFixed._api.logging().error());
        }
        colorCombobox.addActionListener(l -> updateFilter());

        // Protocal combobox
        protocolCombobox.addActionListener(l -> updateFilter());


        /*******************
         * Logs table setup
         *******************/
        // Setup table model for Log Table
        oastTable.setModel(oastTableModel);
        OASTTableModel.setColumnWidth(oastTable);
        // Set Bold font for unreaded row
        OASTTableModel.setCellRenderer(oastTable);

        // Log table selection change event
        oastTable.getSelectionModel().addListSelectionListener(e -> oastTableSelectionChange());

        // Log table popup menu
        JPopupMenu logPopupmenu = new JPopupMenu();

        JMenuItem addCommentMenu = new JMenuItem("Add comment");
        addCommentMenu.addActionListener(l -> addLogComment());
        logPopupmenu.add(addCommentMenu);

        JMenu changeColorMenu = new JMenu("Highlight");
        for (String color : colors) {
            Action menuAction = new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        changeLogColor((String)this.getValue(Action.NAME));
                    }catch (Exception ex){
                        ex.printStackTrace(OASTFixed._api.logging().error());
                    };
                }
            };

            menuAction.putValue(Action.NAME, color);

            JMenuItem menu = new JMenuItem("      ");;

            if (!Objects.isNull(color)){
                try {
                    menu.setOpaque(true);
                    menu.setBackground((Color) (Color.class.getField(color).get(null)));
                }catch (Exception ex){};
            }

            menu.addActionListener(menuAction);
            changeColorMenu.add(menu);
        }
        logPopupmenu.add(changeColorMenu);

        logPopupmenu.addSeparator();
        JMenuItem removeSelectedLogMenu = new JMenuItem("Remove selected interaction");
        removeSelectedLogMenu.addActionListener(l -> removeSelectedlog());
        logPopupmenu.add(removeSelectedLogMenu);

        logPopupmenu.addSeparator();
        JMenuItem clearLogMenu = new JMenuItem("Clear interaction history");
        clearLogMenu.addActionListener(l -> clearlog());
        logPopupmenu.add(clearLogMenu);

        oastTable.setComponentPopupMenu(logPopupmenu);


        /**********************************
         * Server and Payload tables setup
         **********************************/
        // Set up config tables
        serverTable.setModel(new ServerTableModel());
        ServerTableModel.setColumnWidth(serverTable);
        oastServerListDetailLabel.putClientProperty("html.disable", Boolean.FALSE);
        payloadTable.setModel(new PayloadTableModel());

        // OAST Server table
        JPopupMenu serverPopupmenu = new JPopupMenu();
        JMenuItem disableServerMenu = new JMenuItem("Enable / Disable server");
        disableServerMenu.addActionListener(l -> enableDisableServer());
        serverPopupmenu.add(disableServerMenu);
        JMenuItem setServerToBurpConfig = new JMenuItem("Set server to Burp Collaborator config");
        setServerToBurpConfig.addActionListener(l -> setServerToBurp());
        serverPopupmenu.add(setServerToBurpConfig);
        serverTable.setComponentPopupMenu(serverPopupmenu);

        // Set STRIKETHROUGH style for disabled server
        serverTable.setDefaultRenderer(Object.class, new ServerTableModel.ServerTableCellRenderer());

        // OAST server buttons
        addServerButton.addActionListener(l -> addServer());
        editServerButton.addActionListener(l -> editServer());
        removeServerButton.addActionListener(l -> removeServer());

        // Payload buttons
        addPayloadButton.addActionListener(l -> addPayload());
        editPayloadButton.addActionListener(l -> editPayload());
        copyPayloadButton.addActionListener(l -> copyPayload());
        removePayloadButton.addActionListener(l -> removePayload());

        changeFolderButton.addActionListener(l -> changeLogsFolder());

        // Setup auto poll
        autoPollSpinner.setValue(60);
        autoPollThread.start();
    }

    /*************************
     * Logs table functions
     *************************/
    void poll() {
        pollButton.setEnabled(false);
        new Thread(() -> {
            oastTableModel.poll();
            pollButton.setEnabled(true);
        }).start();
        if (!isAddRootPaneChangeEvent) {
            JTabbedPane rootTabbedPane = (JTabbedPane) OASTFixed.oastFixedTab.mainPanel.getParent();
            rootTabbedPane.addChangeListener(l -> rootPaneSelectionChange());
            isAddRootPaneChangeEvent = true;
        }
    }

    void detailTabbedPaneSelectionChange() {
        if (detailTabbedPane.getTitleAt(detailTabbedPane.getSelectedIndex()).equals("Ip Info"))
            if (ipInfoEditor.getContents().length() == 0) {
                int result = JOptionPane.showConfirmDialog(OASTFixed._api.userInterface().swingUtils().suiteFrame(),
                        "The IPInfo of this IP has not been loaded, do you want to get it?",
                        "Load IPInfo",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (result == JOptionPane.YES_OPTION) {
                    int selectedId = Integer.parseInt(oastTable.getValueAt(oastTable.getSelectedRow(), 0).toString()) - 1;
                    OASTObj currentObj = oastList.stream().filter(v -> v._id == selectedId).findFirst().get();

                    queryIp(IpInfoUtils.getIpFromIpAndPort(currentObj._sourceIPAndPORT));

                    ipInfoEditor.setContents(ByteArray.byteArray(currentObj.getIpInfo()));

                    DBUtils.saveIpInfo();
                }
            }
    }

    void oastTableSelectionChange() {
        if (oastTable.getSelectedRow() == -1)
            return;

        detailTabbedPane.removeChangeListener(detailTabbedPaneChangeListener);

        int selectedId = Integer.parseInt(oastTable.getValueAt(oastTable.getSelectedRow(), 0).toString()) - 1;
        OASTObj currentObj = oastList.stream().filter(v -> v._id == selectedId).findFirst().get();
        int tabSelectedIndex = detailTabbedPane.getSelectedIndex();
        cleanupDetailTabs();

        if (currentObj.getClass() == DNSObj.class) {
            if (currentObj._serverType == OASTServerService.Interactsh) {
                requestEditor.setRequest(HttpRequest.httpRequest(ByteArray.byteArray(((DNSObj) currentObj)._rawRequest)));
                responseEditor.setResponse(HttpResponse.httpResponse(ByteArray.byteArray(((DNSObj) currentObj)._rawResponse)));
                detailTabbedPane.add("Request to OAST Server", requestEditor.uiComponent());
                detailTabbedPane.add("Response from OAST Server", responseEditor.uiComponent());
            } else { // if (currentObj._serverType == OASTServerService.BurpCollab) {
                requestEditor.setRequest(HttpRequest.httpRequest(ByteArray.byteArray(((DNSObj) currentObj)._rawQuery)));
                detailTabbedPane.add("Raw DNS Query", requestEditor.uiComponent());
            }
        } else if (currentObj.getClass() == HTTPObj.class) {
            requestEditor.setRequest(HttpRequest.httpRequest(ByteArray.byteArray(((HTTPObj) currentObj)._rawRequest)));
            responseEditor.setResponse(HttpResponse.httpResponse(ByteArray.byteArray(((HTTPObj) currentObj)._rawResponse)));
            detailTabbedPane.add("Request to OAST Server", requestEditor.uiComponent());
            detailTabbedPane.add("Response from OAST Server", responseEditor.uiComponent());
        } else if (currentObj.getClass() == SMTPObj.class) {
            rawRequestEditor.setContents(ByteArray.byteArray(((SMTPObj) currentObj)._rawRequest));
            detailTabbedPane.add("SMTP Conversation", rawRequestEditor.uiComponent());
        } else if (currentObj.getClass() == FTPObj.class) {
            rawRequestEditor.setContents(ByteArray.byteArray(((FTPObj) currentObj)._rawRequest));
            detailTabbedPane.add("FTP Raw request", rawRequestEditor.uiComponent());
        } else if (currentObj.getClass() == LDAPObj.class) {
            rawRequestEditor.setContents(ByteArray.byteArray(((LDAPObj) currentObj)._rawRequest));
            detailTabbedPane.add("LDAP Raw request", rawRequestEditor.uiComponent());
        } else {
            requestEditor.setRequest(HttpRequest.httpRequest("GET /ignore_this_line HTTP/2\n\n" + ((OASTUnknownObj) currentObj)._data));
            detailTabbedPane.add("Raw Data", requestEditor.uiComponent());
        }

        descriptionTextPane.setText(currentObj.description());

        if (!currentObj._sourceIPAndPORT.isEmpty()) {
            ipInfoEditor.setContents(ByteArray.byteArray(currentObj.getIpInfo()));
            detailTabbedPane.add("Ip Info", ipInfoEditor.uiComponent());
        }

        detailTabbedPane.setSelectedIndex(tabSelectedIndex);
        detailTabbedPane.addChangeListener(detailTabbedPaneChangeListener);
    }

    void removeSelectedlog() {
        int result = JOptionPane.showConfirmDialog(OASTFixed._api.userInterface().swingUtils().suiteFrame(),
                "Are you sure to remove selected interaction?",
                "Remove interaction",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (result == JOptionPane.YES_OPTION) {
            int[] selectedRows = oastTable.getSelectedRows();

            ArrayList<Integer> selectedIds = new ArrayList<>();

            for (int i : selectedRows) {
                int selectedId = Integer.parseInt(oastTable.getValueAt(i, 0).toString()) - 1;
                selectedIds.add(selectedId);
                OASTFixed._api.logging().logToOutput("Remove log " + i + " " + selectedId);
            }

            oastList.removeIf(o -> selectedIds.contains(o._id));

            ((AbstractTableModel) oastTable.getModel()).fireTableDataChanged();
            DBUtils.saveLogs();
        }
    }

    void clearlog() {
        int result = JOptionPane.showConfirmDialog(OASTFixed._api.userInterface().swingUtils().suiteFrame(),
                "Are you sure to clear all of log?",
                "Clear log",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (result == JOptionPane.YES_OPTION) {
            oastList.clear();
            ((AbstractTableModel) oastTable.getModel()).fireTableDataChanged();
            DBUtils.saveLogs();
        }
    }

    void addLogComment() {
        try {
            String initComment = null;
            List<OASTObj> oastObjs = new ArrayList<>();

            int[] selectedRows = oastTable.getSelectedRows();
            for (int i = 0; i < selectedRows.length; ++i) {
                int selectedId = Integer.parseInt(oastTable.getValueAt(selectedRows[i], 0).toString()) - 1;
                oastObjs.add(oastList.stream().filter(v -> v._id == selectedId).findFirst().get());

                // Compare comments
                if (Objects.isNull(initComment))
                    initComment = oastObjs.get(i)._comment;
                else {
                    if (!initComment.equals(oastObjs.get(i)._comment))
                        initComment = "[Different values]";
                }
            }
            
            // Input and save comment
            String newComment = JOptionPane.showInputDialog("Enter a comment", initComment);
            if (!Objects.isNull(newComment)) {
                for (int i = 0; i < selectedRows.length; ++i) {
                    oastObjs.get(i)._comment = newComment;
                }
                ((AbstractTableModel) oastTable.getModel()).fireTableDataChanged();
                DBUtils.saveLogs();
            }
        } catch (Exception ex) {
            ex.printStackTrace(OASTFixed._api.logging().error());
        }
    }

    void changeLogColor(String color) {
        int[] selectedRows = oastTable.getSelectedRows();
        for (int i : selectedRows) {
            int selectedId = Integer.parseInt(oastTable.getValueAt(i, 0).toString()) - 1;
            OASTObj currentObj = oastList.stream().filter(v -> v._id == selectedId).findFirst().get();
            currentObj._color = color;
        }
        DBUtils.saveLogs();
    }


    /*****************************
     * OAST Server List functions
     *****************************/
    void addServer() {
        AddServerWindow addServerWindow = new AddServerWindow();
        addServerWindow.setTitle("Add new OAST Server");
        addServerWindow.buttonOKBurpCollab.setText("Add");
        addServerWindow.buttonOKInteractsh.setText("Add");
        addServerWindow.setVisible(true);
    }

    void editServer(){
        if (serverTable.getSelectedRow() == -1)
            return;

        OASTClient oastClient = oastClients.get(serverTable.getSelectedRow());

        AddServerWindow addServerWindow = new AddServerWindow();
        addServerWindow.setTitle("Edit OAST Server");

        // Burp Collab
        if (oastClient instanceof BClient bClient) {
            addServerWindow.buttonOKBurpCollab.setText("Save");
            addServerWindow.burpNameTextField.setText(bClient._name);
            addServerWindow.defaultBurpCheckBox.setSelected(false);
            addServerWindow.burpServerLocation.setText(bClient._serverLocation);
            addServerWindow.burpPollingLocation.setText(bClient._pollLocation);
            addServerWindow.biidTextField.setText(bClient._secrect);

            // Disable edit server location, biid
            addServerWindow.defaultBurpCheckBox.setVisible(false);
            addServerWindow.defaultBurpCombobox.setVisible(false);
            addServerWindow.burpServerLocation.setEditable(false);
            addServerWindow.biidTextField.setEditable(false);
            addServerWindow.burpBiidRandomButton.setEnabled(false);

            addServerWindow.topCardLayout.remove(0);

        // Interactsh
        } else if (oastClient instanceof IClient iClient) {
            addServerWindow.buttonOKInteractsh.setText("Save");
            addServerWindow.interactshNameTextField.setText(iClient._name);
            addServerWindow.defaultInteractshCheckbox.setSelected(false);
            addServerWindow.interactshServerTextField.setText(iClient._serverLocation);
            addServerWindow.interactshTokenTextField.setText(iClient._token);
            addServerWindow.interactshCorrIDTextField.setText(iClient._correlationId);
            addServerWindow.enableWildcardSubdomainCheckBox.setSelected(iClient._isEnableWildcardSubdomain);

            // Disable edit server location, correlation id
            addServerWindow.defaultInteractshCheckbox.setVisible(false);
            addServerWindow.defaultInteractshCombobox.setVisible(false);
            addServerWindow.interactshServerTextField.setEditable(false);
            addServerWindow.interactshCorrIDTextField.setEditable(false);
            addServerWindow.interactshRandomButton.setEnabled(false);

            addServerWindow.topCardLayout.remove(0);
            addServerWindow.topCardLayout.remove(0);
        }
        else {
            addServerWindow.dispose();
            return;
        }
        addServerWindow.setVisible(true);
    }

    void removeServer() {
        int selectedRow = serverTable.getSelectedRow();
        if (selectedRow > -1) {
            int result = JOptionPane.showConfirmDialog(OASTFixed._api.userInterface().swingUtils().suiteFrame(),
                    "Are you sure to remove server \"" + oastClients.get(selectedRow)._name + "\" and all of its payloads?",
                    "Remove OAST server",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (result == JOptionPane.YES_OPTION) {
                oastPayloads.removeIf(payload -> payload._serverId == oastClients.get(selectedRow)._id);
                ((AbstractTableModel) payloadTable.getModel()).fireTableDataChanged();
                oastClients.remove(selectedRow);
                ((AbstractTableModel) serverTable.getModel()).fireTableRowsDeleted(selectedRow, selectedRow);
            }
        }
    }

    void enableDisableServer() {
        OASTClient client = oastClients.get(serverTable.getSelectedRow());
        client._isEnable = !client._isEnable;
        DBUtils.saveServers();
    }

    void setServerToBurp() {
        OASTClient client = oastClients.get(serverTable.getSelectedRow());
        if (!(client instanceof BClient))
            JOptionPane.showMessageDialog(OASTFixed._api.userInterface().swingUtils().suiteFrame(), "This action only for Burp Collaborator", "Error", JOptionPane.WARNING_MESSAGE);
        else {
            ((BClient) client).updateConfig();
            JOptionPane.showMessageDialog(OASTFixed._api.userInterface().swingUtils().suiteFrame(), "Success setup Burp Collaborator", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
    }


    /**************************
     * Payload Table functions
     **************************/
    void addPayload() {
        AddPayloadWindow addPayloadWindow = new AddPayloadWindow();
        addPayloadWindow.setTitle("Add new Payload");
        addPayloadWindow.buttonOK.setText("Add");
        addPayloadWindow.setVisible(true);
    }

    void editPayload() {
        OASTPayload oastPayload = oastPayloads.get(payloadTable.getSelectedRow());

        AddPayloadWindow addPayloadWindow = new AddPayloadWindow();
        addPayloadWindow.setTitle("Edit Payload");
        addPayloadWindow.buttonOK.setText("Save");

        addPayloadWindow.nameTextField.setText(oastPayload._name);
        addPayloadWindow.payloadTextField.setText(oastPayload._payload);
        addPayloadWindow.payloadTextField.setEditable(false);
        addPayloadWindow.randomButton.setEnabled(false);
        addPayloadWindow.serverCombobox.setSelectedItem(oastClients.stream().filter(v -> v._id == oastPayload._serverId).findFirst().get());
        addPayloadWindow.serverCombobox.setEnabled(false);

        addPayloadWindow.pack();
        addPayloadWindow.setLocationRelativeTo(null);
        addPayloadWindow.setVisible(true);
    }

    void copyPayload() {
        StringSelection selection = new StringSelection(oastPayloads.get(payloadTable.getSelectedRow()).toString());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
    }

    void removePayload() {
        int selectedRow = payloadTable.getSelectedRow();
        if (selectedRow > -1) {
            int result = JOptionPane.showConfirmDialog(OASTFixed._api.userInterface().swingUtils().suiteFrame(),
                    "Are you sure to remove payload \"" + oastPayloads.get(selectedRow)._name + "\"?",
                    "Remove Payload",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (result == JOptionPane.YES_OPTION) {
                oastPayloads.remove(selectedRow);
                ((AbstractTableModel) payloadTable.getModel()).fireTableRowsDeleted(selectedRow, selectedRow);
                DBUtils.savePayloads();
            }
        }
    }


    /*******************
     * Config functions
     *******************/
    void changeLogsFolder() {
        String newPath = JOptionPane.showInputDialog("New folder path to save logs: ");
        if (!Objects.isNull(newPath)) {
            File f = new File(newPath);
            if (f.exists() && f.isDirectory()) {
                logsFolderTextField.setText(newPath);
                DBUtils.saveLogs();
                DBUtils.saveServers();
                DBUtils.savePayloads();
                DBUtils.saveConfig();
            } else {
                JOptionPane.showMessageDialog(OASTFixed._api.userInterface().swingUtils().suiteFrame(), "Folder not exists.", "Invalid path", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    void queryIp(String ip) {
        if (!ip.isEmpty() && !ipInfoMap.containsKey(ip)) {
            String ipinfo = IpInfoUtils.getIpInfo(ip, new String(ipInfoTKField.getPassword()));
            if (!ipinfo.isEmpty())
                ipInfoMap.put(ip, ipinfo);
        }
    }

    void queryAllIpInfo() {
        queryAllIPButton.setEnabled(false);
        new Thread(() -> {
            for (OASTObj oastObj : oastList) {
                String ip = IpInfoUtils.getIpFromIpAndPort(oastObj._sourceIPAndPORT);
                queryIp(ip);
            }
            DBUtils.saveIpInfo();
            queryAllIPButton.setEnabled(true);
        }).start();
    }

    void changeIPToken() {
        String newtoken = JOptionPane.showInputDialog("New IPInfo Token: ");
        if (!Objects.isNull(newtoken)) {
            ipInfoTKField.setText(newtoken);
            DBUtils.saveConfig();
        }
    }

    static public void rootPaneSelectionChange() {
        JTabbedPane rootTabbedPane = (JTabbedPane) OASTFixed.oastFixedTab.mainPanel.getParent();
        if (rootTabbedPane.getTitleAt(rootTabbedPane.getSelectedIndex()).equals(OASTFixed.extension_name))
            resetHighlightTab();
    }

    void cleanupDetailTabs() {
        while (detailTabbedPane.getTabCount() > 1) {
            detailTabbedPane.remove(1);
        }
    }

    static public void highlightTab() {
        JTabbedPane rootTabbedPane = (JTabbedPane) OASTFixed.oastFixedTab.mainPanel.getParent();
        for (int i = 0; i < rootTabbedPane.getTabCount(); ++i) {
            if (rootTabbedPane.getTitleAt(i).equals(OASTFixed.extension_name)) {
                rootTabbedPane.setBackgroundAt(i, Color.red);
            }
        }
    }

    static public void resetHighlightTab() {
        JTabbedPane rootTabbedPane = (JTabbedPane) OASTFixed.oastFixedTab.mainPanel.getParent();
        for (int i = 0; i < rootTabbedPane.getTabCount(); ++i) {
            if (rootTabbedPane.getTitleAt(i).equals(OASTFixed.extension_name)) {
                rootTabbedPane.setBackgroundAt(i, OASTFixed._api.userInterface().currentTheme().equals(Theme.LIGHT) ? Color.black : new Color(187,187,187));
            }
        }
    }


    /*******************
     * Filter functions
     *******************/
    void updateFilter() {
        try {
            String queryText = logSearchTextField.getText();
            Object colorSelected = colorCombobox.getSelectedItem();
            String protocolSelected = (String) protocolCombobox.getSelectedItem();

            // Change color combobox background
            if (colorSelected instanceof Integer)
                colorCombobox.setBackground(OASTObj.getColor(0,null,false));
            else
                colorCombobox.setBackground(OASTObj.getColor(0,(String) colorSelected,false));

            if (queryText.isEmpty() && protocolSelected.isEmpty() && colorSelected instanceof Integer)
                clearFilter();
            else {
                List<RowFilter<Object, Object>> filters = new ArrayList<>();

                // Add filter from seach box
                if (!queryText.isEmpty())
                    filters.add(new RowFilter() {
                        @Override
                        public boolean include(Entry entry) {
                            int selectedId = (int) entry.getValue(0) - 1;
                            OASTObj currentObj = OASTFixed.oastFixedTab.oastList.stream().filter(v -> v._id == selectedId).findFirst().get();
                            if (currentObj._timestamp.contains(queryText))
                                return true;
                            if (currentObj._server.contains(queryText))
                                return true;
                            if (currentObj._payload.contains(queryText))
                                return true;
                            if (currentObj._sourceIPAndPORT.contains(queryText))
                                return true;
                            if (currentObj._comment.contains(queryText))
                                return true;

                            // Check value for Object types
                            if (currentObj instanceof DNSObj) {
                                if (currentObj.description().contains(queryText))
                                    return true;
                            } else if (currentObj instanceof HTTPObj) {
                                HTTPObj httpObj = (HTTPObj) currentObj;
                                if (OASTFixed._api.utilities().byteUtils().indexOf(httpObj._rawRequest, queryText.getBytes()) > -1)
                                    return true;
                                else if (OASTFixed._api.utilities().byteUtils().indexOf(httpObj._rawResponse, queryText.getBytes()) > -1)
                                    return true;
                            } else if (currentObj instanceof SMTPObj) {
                                SMTPObj smtpObj = (SMTPObj) currentObj;
                                if (smtpObj._rawRequest.contains(queryText))
                                    return true;
                            } else if (currentObj instanceof FTPObj) {
                                FTPObj ftpObj = (FTPObj) currentObj;
                                if (ftpObj._rawRequest.contains(queryText))
                                    return true;
                            } else if (currentObj instanceof LDAPObj) {
                                LDAPObj ldapObj = (LDAPObj) currentObj;
                                if (ldapObj._rawRequest.contains(queryText))
                                    return true;
                            } else if (currentObj instanceof OASTUnknownObj) {
                                OASTUnknownObj unknownObj = (OASTUnknownObj) currentObj;
                                if (unknownObj._data.contains(queryText))
                                    return true;
                            }

                            // Check IPInfo
                            if (currentObj.getIpInfo().contains(queryText)) {
                                return true;
                            }

                            return false;
                        }
                    });

                // Add Color filter
                if (!(colorSelected instanceof Integer))
                    filters.add(new RowFilter() {
                        @Override
                        public boolean include(Entry entry) {
                            int selectedId = (int) entry.getValue(0) - 1;
                            OASTObj currentObj = OASTFixed.oastFixedTab.oastList.stream().filter(v -> v._id == selectedId).findFirst().get();
                            if (Objects.isNull(colorSelected))
                                return Objects.isNull(currentObj._color);
                            else
                                return colorSelected.equals(currentObj._color);
                        }
                    });

                // Add Protocol filter
                if (!protocolSelected.isEmpty())
                    filters.add(RowFilter.regexFilter(protocolSelected, 2));

                // Apply filter
                ((TableRowSorter) oastTable.getRowSorter()).setRowFilter(RowFilter.andFilter(filters));
            }
        } catch (Exception ex) {
            ex.printStackTrace(OASTFixed._api.logging().error());
        }
    }

    void clearFilter() {
        ((TableRowSorter) oastTable.getRowSorter()).setRowFilter(null);
        logSearchTextField.setText("");
        colorCombobox.setSelectedIndex(0);
        protocolCombobox.setSelectedIndex(0);
    }
}
