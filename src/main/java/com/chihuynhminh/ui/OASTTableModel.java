package com.chihuynhminh.ui;

import burp.api.montoya.ui.Theme;
import com.chihuynhminh.Client.OASTClient;
import com.chihuynhminh.OASTFixed;
import com.chihuynhminh.Obj.OASTObj;
import com.chihuynhminh.utils.DBUtils;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.*;
import java.util.List;

public class OASTTableModel extends AbstractTableModel {
    static String[] columnName = {"#", "Time", "Protocol", "Server", "Payload", "Source IP and Port", "Comment", "Server Type" };
    static int[] columnWidth = { 5, 200, 50, 100, 300, 100, 500, 50 };

    public void poll() {
        try {
            for (OASTClient client : OASTFixed.oastFixedTab.oastClients) {
                if (!client._isEnable)
                    continue;

                List<OASTObj> pollList;
                try {
                    pollList = client.poll();
                } catch (Exception ex) {
                    ex.printStackTrace(OASTFixed._api.logging().error());
                    continue;
                }

                if (pollList.size() > 0) {
                    OASTFixedTab.highlightTab();
                    for (OASTObj oastObj : pollList) {
                        Optional<OASTObj> oastObj1 = OASTFixed.oastFixedTab.oastList.stream().max(Comparator.comparing(v -> v._id));
                        oastObj._id = oastObj1.map(obj -> obj._id + 1).orElse(0);
                        OASTFixed.oastFixedTab.oastList.add(oastObj);
                    }

                    fireTableRowsInserted(OASTFixed.oastFixedTab.oastList.size() - pollList.size(), OASTFixed.oastFixedTab.oastList.size() - 1);
                    DBUtils.saveLogs();
                    OASTFixed.oastFixedTab.oastTable.scrollRectToVisible(OASTFixed.oastFixedTab.oastTable.getCellRect(OASTFixed.oastFixedTab.oastTable.getRowCount() - 1, 0, true));
                }
            }
        }
        catch (Exception ex){
            ex.printStackTrace(OASTFixed._api.logging().error());
        }
    }

    @Override
    public int getRowCount() {
        return OASTFixed.oastFixedTab.oastList.size();
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return OASTFixed.oastFixedTab.oastList.get(rowIndex)._id + 1;
            case 1:
                return OASTFixed.oastFixedTab.oastList.get(rowIndex)._timestamp;
            case 2:
                return OASTFixed.oastFixedTab.oastList.get(rowIndex)._protocol;
            case 3:
                return OASTFixed.oastFixedTab.oastList.get(rowIndex)._server;
            case 4:
                return OASTFixed.oastFixedTab.oastList.get(rowIndex)._payload;
            case 5:
                return OASTFixed.oastFixedTab.oastList.get(rowIndex)._sourceIPAndPORT;
            case 6:
                return OASTFixed.oastFixedTab.oastList.get(rowIndex)._comment;
            default:
                return OASTFixed.oastFixedTab.oastList.get(rowIndex)._serverType.name();
        }
    }

    @Override
    public String getColumnName(int columnIndex){
        if (columnIndex < columnName.length) {
            return columnName[columnIndex];
        }
        return "";
    }

    @Override
    public Class getColumnClass(int column) {
        if (column == 0)
            return Integer.class;
        return String.class;
    }

    static public void setColumnWidth(JTable table){
        for (int i = 0; i < columnWidth.length; ++i) {
            table.getColumnModel().getColumn(i).setPreferredWidth(columnWidth[i]);
        }
    }


    ///////////////////////////////////
    // Set unreaded row font to bold //
    ///////////////////////////////////
    static public void setCellRenderer(JTable table) {
        table.setDefaultRenderer(String.class, new OASTTableCellRenderer());
        table.setDefaultRenderer(Integer.class, new OASTTableCellRenderer());

        table.getSelectionModel().addListSelectionListener(e -> {
            if (table.getSelectedRow() == -1)
                return;

            // Change font to normal
            int selectedId = Integer.parseInt(table.getValueAt(table.getSelectedRow(), 0).toString()) - 1;
            OASTObj currentObj = OASTFixed.oastFixedTab.oastList.stream().filter(v -> v._id == selectedId).findFirst().get();

            if (!currentObj._isReaded) {
                currentObj._isReaded = true;
                DBUtils.saveLogs();
                OASTFixedTab.resetHighlightTab();
            }
        });
    }

    static private class OASTTableCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                       boolean hasFocus, int row, int column) {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            int selectedId = Integer.parseInt(OASTFixed.oastFixedTab.oastTable.getValueAt(row, 0).toString()) - 1;
            OASTObj currentObj = OASTFixed.oastFixedTab.oastList.stream().filter(v -> v._id == selectedId).findFirst().get();
            if (!currentObj._isReaded) {
                this.setFont(this.getFont().deriveFont(Font.BOLD));
            }

            // Set Foreground for dark theme
            if (Objects.isNull(currentObj._color)) {
                if (OASTFixed._api.userInterface().currentTheme().equals(Theme.DARK))
                    this.setForeground(new Color(187,187,187));
                else
                    this.setForeground(Color.black);
            } else if (currentObj._color.equals("red") || currentObj._color.equals("blue"))
                this.setForeground(Color.white);
            else
                this.setForeground(Color.black);

            // Set color
            try{
                this.setBackground(OASTObj.getColor(row, currentObj._color, isSelected));
            } catch (Exception ex) {
                ex.printStackTrace(OASTFixed._api.logging().error());
            }
            return this;
        }
    }
}
