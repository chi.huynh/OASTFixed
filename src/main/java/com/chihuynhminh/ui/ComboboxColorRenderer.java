package com.chihuynhminh.ui;

import com.chihuynhminh.OASTFixed;
import com.chihuynhminh.Obj.OASTObj;

import javax.swing.*;
import java.awt.*;

public class ComboboxColorRenderer extends DefaultListCellRenderer {
    @Override
    public Component getListCellRendererComponent(JList list,
                                                  Object value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {
        try {
            if (value instanceof Integer) {
                setText("ALL");
                setBackground(OASTObj.getColor(0, null, isSelected));
            } else {
                setText(" ");
                setBackground(OASTObj.getColor(0, (String) value, isSelected));
            }
        } catch (Exception e) {
            e.printStackTrace(OASTFixed._api.logging().error());
        }
        return this;
    }
}
