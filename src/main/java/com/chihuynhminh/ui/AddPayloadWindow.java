package com.chihuynhminh.ui;

import com.chihuynhminh.Client.OASTClient;
import com.chihuynhminh.Client.OASTPayload;
import com.chihuynhminh.OASTFixed;
import com.chihuynhminh.utils.DBUtils;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.Comparator;
import java.util.Optional;

public class AddPayloadWindow extends JDialog {
    private JPanel contentPane;
    public JButton buttonOK;
    private JButton buttonCancel;
    public JTextField nameTextField;
    public JComboBox<OASTClient> serverCombobox;
    public JButton randomButton;
    public JTextField payloadTextField;

    public AddPayloadWindow() {
        setContentPane(contentPane);
        setModal(true);

        buttonOK.addActionListener(l -> onOK());
        buttonCancel.addActionListener(l -> dispose());
        randomButton.addActionListener(l -> randomPayload());

        for (OASTClient client: OASTFixed.oastFixedTab.oastClients){
            serverCombobox.addItem(client);
        }

        serverCombobox.addActionListener(l -> {
            if (payloadTextField.getText().equals(finalRandomPayload))
                randomPayload();
        });

        // Resize window to fixed with components
        pack();
        // Set location of window at center of screen
        setLocationRelativeTo(null);
    }

    void onOK() {
        // Check if empty name
        if (nameTextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(OASTFixed._api.userInterface().swingUtils().suiteFrame(), "Please input name of payload.", "Input name", JOptionPane.WARNING_MESSAGE);
            return;
        }

        // Add new Payload
        if (buttonOK.getText().equals("Add")) {
            // If payload field is empty, generate new random
            if (payloadTextField.getText().isEmpty()) {
                randomPayload();
                // Warning custom payload not from random button
            } else {
                if (!payloadTextField.getText().equals(finalRandomPayload)) {
                    int result = JOptionPane.showConfirmDialog(OASTFixed._api.userInterface().swingUtils().suiteFrame(),
                            """
                                    Payload you add not from the random button.
                                    You must make sure this Payload match to the above server.
                                    Do you want to add this payload?""",
                            "Custom payload",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE);
                    if (result == JOptionPane.NO_OPTION)
                        return;
                }
            }

            // Get the next id
            Optional<OASTPayload> oastClient = OASTFixed.oastFixedTab.oastPayloads.stream().max(Comparator.comparing(v -> v._id));
            int newId = oastClient.map(client -> client._id + 1).orElse(0);

            // Add new payload to the table
            OASTPayload oastPayload = new OASTPayload(newId, ((OASTClient) serverCombobox.getSelectedItem())._id, nameTextField.getText(), payloadTextField.getText());
            OASTFixed.oastFixedTab.oastPayloads.add(oastPayload);
            ((AbstractTableModel) OASTFixed.oastFixedTab.payloadTable.getModel()).fireTableRowsInserted(OASTFixed.oastFixedTab.oastPayloads.size() - 1, OASTFixed.oastFixedTab.oastPayloads.size() - 1);

            // Close window
            dispose();

        // Edit payload - Only allow edit name
        } else {
            int selectedIndex = OASTFixed.oastFixedTab.payloadTable.getSelectedRow();
            OASTPayload oastPayload = OASTFixed.oastFixedTab.oastPayloads.get(selectedIndex);
            oastPayload._name = nameTextField.getText();
            ((AbstractTableModel) OASTFixed.oastFixedTab.payloadTable.getModel()).fireTableRowsInserted(selectedIndex, selectedIndex);
        }

        // Save payload to DB
        DBUtils.savePayloads();

        // Close window
        dispose();
    }

    public String finalRandomPayload;

    void randomPayload() {
        OASTClient client = (OASTClient) serverCombobox.getSelectedItem();
        finalRandomPayload = client.randomPayload();
        payloadTextField.setText(finalRandomPayload);
    }
}
