package com.chihuynhminh.ui;

import com.chihuynhminh.Client.BClient;
import com.chihuynhminh.OASTFixed;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.font.TextAttribute;
import java.util.Map;

public class ServerTableModel extends AbstractTableModel {
    static String[] columnName = {"#", "Name", "Server Location", "Server Type" };
    static int[] columnWidth = { 10, 200, 200, 200 };

    @Override
    public int getRowCount() {
        return OASTFixed.oastFixedTab.oastClients.size();
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return OASTFixed.oastFixedTab.oastClients.get(rowIndex)._id + 1;
            case 1:
                return OASTFixed.oastFixedTab.oastClients.get(rowIndex)._name;
            case 2:
                return OASTFixed.oastFixedTab.oastClients.get(rowIndex)._serverLocation;
            default:
                if (OASTFixed.oastFixedTab.oastClients.get(rowIndex) instanceof BClient)
                    return "Burp Collaborator";
                else
                    return "Interactsh";
        }
    }

    @Override
    public String getColumnName(int columnIndex){
        if (columnIndex < columnName.length) {
            return columnName[columnIndex];
        }
        return "";
    }

    static public void setColumnWidth(JTable table){
        for (int i = 0; i < columnWidth.length; ++i) {
            table.getColumnModel().getColumn(i).setPreferredWidth(columnWidth[i]);
        }
    }

    static public class ServerTableCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                       boolean hasFocus, int row, int column) {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (!OASTFixed.oastFixedTab.oastClients.get(row)._isEnable) {
                Font font = this.getFont();
                Map attributes = font.getAttributes();
                attributes.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON);
                this.setFont(new Font(attributes));
            }
            return this;
        }
    }
}
