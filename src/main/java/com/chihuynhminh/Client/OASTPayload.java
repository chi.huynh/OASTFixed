package com.chihuynhminh.Client;

import com.chihuynhminh.OASTFixed;

import java.io.Serializable;

public class OASTPayload implements Serializable {
    private static final long serialVersionUID = -3523107561170148790L;
    public int _id;
    public int _serverId;
    public String _name;
    public String _payload;

    public OASTPayload(int id, int serverId, String name, String payload) {
        _id = id;
        _serverId = serverId;
        _name = name;
        _payload = payload;
    }

    @Override
    public String toString() {
        OASTClient client = OASTFixed.oastFixedTab.oastClients.stream().filter(cl -> cl._id == this._serverId).findFirst().get();
        return _payload + "." + client._serverLocation;
    }
}
