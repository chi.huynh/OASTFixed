package com.chihuynhminh.Client;

import burp.api.montoya.collaborator.*;
import burp.api.montoya.core.ByteArray;
import burp.api.montoya.http.message.requests.HttpRequest;
import com.chihuynhminh.OASTFixed;
import com.chihuynhminh.Obj.*;
import com.chihuynhminh.utils.DNSUtils;
import com.chihuynhminh.utils.HTTPUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

//Burp Collaborator Client
public class BClient extends OASTClient {
    private static final long serialVersionUID = -7650091649700524920L;
    public String _secrect;
    public String _pollLocation;

    public BClient(String secrect, String serverLocation, String pollLocation, String name) {
        _secrect = secrect;
        _serverLocation = serverLocation;
        _pollLocation = pollLocation;
        _name = name;
    }

    @Override
    public List<OASTObj> poll() throws Exception {
        String pollLocation = _pollLocation;
        if (_serverLocation.equals("oastify.com") || _serverLocation.equals("burpcollaborator.net"))
            pollLocation = "polling.oastify.com";
        else if (_pollLocation.isEmpty())
            pollLocation = _serverLocation;
        String responseBody = HTTPUtils.sendRequest(
                "https://" + pollLocation + "/burpresults?biid=" + URLEncoder.encode(_secrect, StandardCharsets.UTF_8),
                "GET",
                new String[][] {},
                ""
        );

        JSONArray responses;
        JSONObject responseData = new JSONObject(responseBody);
        if (responseData.has("responses"))
                responses = responseData.getJSONArray("responses");
        else
            return new ArrayList<>();

        List<OASTObj> oastList = new ArrayList<>();

        for (int i = 0; i < responses.length(); ++i) {
            OASTObj obj;
            switch (responses.getJSONObject(i).getString("protocol")) {
                case "dns" -> {
                    obj = new DNSObj();
                    obj._protocol = "DNS";
                    ((DNSObj) obj)._queryType = DNSUtils.getQtype(responses.getJSONObject(i).getJSONObject("data").getInt("type"));
                    ((DNSObj) obj)._rawQuery = Base64.getDecoder().decode(responses.getJSONObject(i).getJSONObject("data").getString("rawRequest"));
                    obj._payload = responses.getJSONObject(i).getJSONObject("data").getString("subDomain");
                }
                case "http", "https" -> {
                    obj = new HTTPObj();
                    obj._protocol = responses.getJSONObject(i).getString("protocol").toUpperCase();
                    ((HTTPObj) obj)._rawRequest = Base64.getDecoder().decode(responses.getJSONObject(i).getJSONObject("data").getString("request"));
                    ((HTTPObj) obj)._rawResponse = Base64.getDecoder().decode(responses.getJSONObject(i).getJSONObject("data").getString("response"));

                    try {
                        obj._payload = HttpRequest.httpRequest(ByteArray.byteArray(((HTTPObj) obj)._rawRequest)).headers()
                                .stream().filter(v -> v.name().equalsIgnoreCase("Host")).findFirst().get().value();
                    } catch (Exception ex) {
                        ex.printStackTrace(OASTFixed._api.logging().error());
                        obj._payload = responses.getJSONObject(i).getString("interactionString");
                    };
                }
                case "smtp", "smtps" -> {
                    obj = new SMTPObj();
                    obj._protocol = responses.getJSONObject(i).getString("protocol").toUpperCase();
                    ((SMTPObj) obj)._rawRequest = new String(Base64.getDecoder().decode(responses.getJSONObject(i).getJSONObject("data").getString("conversation")));
                    obj._payload = responses.getJSONObject(i).getString("interactionString");
                }
                default -> {
                    obj = new OASTUnknownObj();
                    ((OASTUnknownObj) obj)._data = responses.getJSONObject(i).toString();
                    obj._payload = responses.getJSONObject(i).getString("interactionString");
                }
            }
            obj._serverType = OASTServerService.BurpCollab;
            obj._timestamp = OASTFixed.dateFormat.format(new Date(Long.parseLong(responses.getJSONObject(i).getString("time"))));
            obj._server = _serverLocation;
            obj._sourceIPAndPORT = responses.getJSONObject(i).getString("client") + ":" + responses.getJSONObject(i).getString("clientPort");
            Optional<OASTPayload> p = OASTFixed.oastFixedTab.oastPayloads.stream().filter(pl -> pl._payload.equals(obj._payload)).findFirst();
            if (p.isPresent())
                obj._comment = p.get()._name;
            else
                obj._comment = "";

            oastList.add(obj);
        }

        return oastList;
    }

    @Override
    public String randomPayload() {
        CollaboratorClient collabClient = OASTFixed._api.collaborator().restoreClient(SecretKey.secretKey(_secrect));
        String current_burp_config = updateConfig();
        String rdPayload = collabClient.generatePayload(PayloadOption.WITHOUT_SERVER_LOCATION).toString();
        resetConfig(current_burp_config);
        return rdPayload;
    }

    public String updateConfig() {
        String current_burp_config = OASTFixed._api.burpSuite().exportProjectOptionsAsJson();

        JSONObject burp_config = new JSONObject(current_burp_config);
        burp_config.getJSONObject("project_options").getJSONObject("misc").getJSONObject("collaborator_server")
                .put("type", "private");
        burp_config.getJSONObject("project_options").getJSONObject("misc").getJSONObject("collaborator_server")
                .put("location", _serverLocation);
        burp_config.getJSONObject("project_options").getJSONObject("misc").getJSONObject("collaborator_server")
                .put("polling_location", _pollLocation);

        // Update config
        OASTFixed._api.burpSuite().importProjectOptionsFromJson(burp_config.toString());

        return current_burp_config;
    }

    static public String enableCollabConfig() {
        String current_burp_config = OASTFixed._api.burpSuite().exportProjectOptionsAsJson();

        JSONObject burp_config = new JSONObject(current_burp_config);
        burp_config.getJSONObject("project_options").getJSONObject("misc").getJSONObject("collaborator_server")
                .put("type", "default");

        // Update config
        OASTFixed._api.burpSuite().importProjectOptionsFromJson(burp_config.toString());

        return current_burp_config;
    }

    static public void resetConfig(String current_burp_config) {
        OASTFixed._api.burpSuite().importProjectOptionsFromJson(current_burp_config);
    }

}
