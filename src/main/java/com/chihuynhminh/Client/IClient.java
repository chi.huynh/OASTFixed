package com.chihuynhminh.Client;

import com.chihuynhminh.OASTFixed;
import com.chihuynhminh.Obj.*;
import com.chihuynhminh.algorithm.AESCipher;
import com.chihuynhminh.algorithm.RSACipher;
import com.chihuynhminh.utils.HTTPUtils;

import java.io.IOException;
import java.io.Serial;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import com.chihuynhminh.utils.RandomUtils;
import org.json.JSONObject;

// InteractSH Client
public class IClient extends OASTClient {
    private static final long serialVersionUID = 5699025989012603327L;
    public String _token;
    public KeyPair _rsaKeyPair;
    public String _correlationId;
    public String _secret;
    public boolean _isEnableWildcardSubdomain;

    public IClient(String itrshHost, String token, KeyPair rsaKeyPair, String correlationId, String secret, String name, boolean isEnableWildcardSubdomain) {
        _serverLocation = itrshHost;
        _token = token;
        if (Objects.isNull(rsaKeyPair))
            _rsaKeyPair = RSACipher.generateRSAKeyPair();
        else
            _rsaKeyPair = rsaKeyPair;
        _correlationId = correlationId;
        if (Objects.isNull(secret))
            _secret = UUID.randomUUID().toString();
        else
            _secret = secret;
        _name = name;
        _isEnableWildcardSubdomain = isEnableWildcardSubdomain;
    }

    public void register() throws Exception {
        String requestBody = "{\"public-key\": \"" + RSACipher.exportPublicKey(_rsaKeyPair.getPublic())
                + "\", \"secret-key\": \"" + _secret
                + "\", \"correlation-id\": \"" + _correlationId + "\"}";

        String responseBody = HTTPUtils.sendRequest(
                "https://" + _serverLocation + "/register",
                "POST",
                new String[][] {{"Content-Type", "application/json"},
                        {"Authorization", _token }},
                requestBody
        );

        if (!responseBody.equals("{\"message\":\"registration successful\"}"))
            throw new Exception("Fail when register new Interactsh client: " + responseBody);
    }

    String api_poll() throws IOException {
        return HTTPUtils.sendRequest(
                "https://" + _serverLocation + "/poll?id=" + _correlationId + "&secret=" + _secret,
                "GET",
                new String[][] {{"Content-Type", "application/json"},
                        {"Authorization", _token }},
                ""
        );
    }

    @Override
    public List<OASTObj> poll() throws Exception {
        String responseBody = api_poll();
        JSONObject responseData = new JSONObject(responseBody);
        if (responseData.has("error")){
            if (responseData.getString("error").equals("could not get interactions: could not get correlation-id from cache")) {
                try {
                    register();

                    // Try poll again after register
                    responseBody = api_poll();
                    responseData = new JSONObject(responseBody);
                    if (responseData.has("error")) {
                        OASTFixed._api.logging().logToError(responseBody);
                        return new ArrayList<>();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace(OASTFixed._api.logging().error());
                    OASTFixed._api.logging().logToError(responseBody);
                    return new ArrayList<>();
                }
            } else {
                OASTFixed._api.logging().logToError(responseBody);
                return new ArrayList<>();
            }
        }
        String encrypted_aes_key = responseData.getString("aes_key");
        String decrypted_aes_key = RSACipher.decryptOAEPKey(_rsaKeyPair.getPrivate(), encrypted_aes_key);

        List<OASTObj> oastList = new ArrayList<>();

        List<String> list_data_string = new ArrayList<>();
        if (!responseData.isNull("data")) {
            for (int i = 0; i < responseData.getJSONArray("data").length(); ++i) {
                String decryptedDataStr = AESCipher.decryptData(responseData.getJSONArray("data").getString(i), decrypted_aes_key);
                list_data_string.add(decryptedDataStr);
            }
        }

        // Poll wildcard subdomain
        if (_isEnableWildcardSubdomain) {
            if (responseData.has("tlddata")) {
                for (int i = 0; i < responseData.getJSONArray("tlddata").length(); ++i) {
                    list_data_string.add(responseData.getJSONArray("tlddata").getString(i));
                }
            }
            if (!responseData.isNull("extra")) {
                for (int i = 0; i < responseData.getJSONArray("extra").length(); ++i) {
                    list_data_string.add(responseData.getJSONArray("extra").getString(i));
                }
            }
        }

        for (String data_string:list_data_string) {
            OASTObj obj;

            try {
                JSONObject data_json = new JSONObject(data_string);
                String protocol = data_json.getString("protocol");
                switch (protocol) {
                    case "http":
                        obj = new HTTPObj();
                        obj._protocol = "HTTP/HTTPS";
                        ((HTTPObj) obj)._rawRequest = data_json.getString("raw-request").getBytes();
                        ((HTTPObj) obj)._rawResponse = data_json.getString("raw-response").getBytes();
                        break;
                    case "dns":
                        obj = new DNSObj();
                        obj._protocol = "DNS";
                        if (data_json.has("q-type"))
                            ((DNSObj) obj)._queryType = data_json.getString("q-type");
                        else
                            ((DNSObj) obj)._queryType = "UNKNOWN";
                        ((DNSObj) obj)._rawRequest = data_json.getString("raw-request");
                        ((DNSObj) obj)._rawResponse = data_json.getString("raw-response");
                        break;
                    case "smtp":
                        obj = new SMTPObj();
                        obj._protocol = "SMTP/SMTPS";
                        ((SMTPObj) obj)._rawRequest = data_json.getString("raw-request");
                        break;
                    case "ftp":
                        obj = new FTPObj();
                        obj._protocol = "FTP";
                        ((FTPObj) obj)._rawRequest = data_json.getString("raw-request");
                        break;
                    case "ldap":
                        obj = new LDAPObj();
                        obj._protocol = "LDAP";
                        ((LDAPObj) obj)._rawRequest = data_json.getString("raw-request");
                        break;
                    default:
                        obj = new OASTUnknownObj();
                        obj._protocol = protocol;
                        ((OASTUnknownObj) obj)._data = data_string;
                }
                obj._serverType = OASTServerService.Interactsh;
                obj._timestamp = data_json.getString("timestamp");
                obj._server = _serverLocation;
                obj._payload = data_json.getString("full-id");
                obj._sourceIPAndPORT = data_json.getString("remote-address");
                obj._comment = "";
            }
            catch (Exception ex) {
                ex.printStackTrace(OASTFixed._api.logging().error());
                obj = new OASTUnknownObj();
                obj._protocol = "UNKNOWN";
                obj._serverType = OASTServerService.Interactsh;
                obj._server = _serverLocation;
                obj._comment = "";
                ((OASTUnknownObj)obj)._data = data_string;
            }

            oastList.add(obj);
        }
        return oastList;
    }

    @Override
    public String randomPayload() {
        String rdString = RandomUtils.genRandomStringFromTime(13);
        return _correlationId + rdString;
    }
}
