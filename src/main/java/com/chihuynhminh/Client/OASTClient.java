package com.chihuynhminh.Client;

import com.chihuynhminh.Obj.OASTObj;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OASTClient implements Serializable {
    private static final long serialVersionUID = -1272845803505814266L;
    public int _id;
    public String _name;
    public String _serverLocation;
    public boolean _isEnable = true;

    public OASTClient() {
        _id = -1;
        _name = "All Servers";
    }

    public List<OASTObj> poll() throws Exception {
        return new ArrayList<>();
    }

    @Override
    public String toString() {
        return _name;
    }

    public String randomPayload() { return ""; }
}
