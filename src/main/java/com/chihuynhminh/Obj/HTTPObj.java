package com.chihuynhminh.Obj;

public class HTTPObj extends OASTObj {
    public byte[] _rawRequest;
    public byte[] _rawResponse;

    @Override
    public String description() {
        String descriptionText = "";
        if (_serverType == OASTServerService.BurpCollab) {
            descriptionText += "The Collaborator server received ";
        } else {
            descriptionText += "The Interactsh server received ";
        }

        descriptionText += "an <b>" + _protocol + "</b> request for the domain name <b>";
        if (_payload.contains(_server))
            descriptionText += _payload;
        else
            descriptionText += _payload + "." + _server;

        return descriptionText + "</b><br><br>The request" + super.description();
    }
}
