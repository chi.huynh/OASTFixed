package com.chihuynhminh.Obj;

import burp.api.montoya.ui.Theme;
import com.chihuynhminh.OASTFixed;
import com.chihuynhminh.utils.IpInfoUtils;
import org.json.JSONObject;

import java.awt.*;
import java.io.Serializable;
import java.util.Objects;

public class OASTObj implements Serializable {
    private static final long serialVersionUID = -7191625062593159055L;
    public int _id;
    public String _timestamp;
    public String _protocol;
    public String _server;
    public String _payload;
    public String _sourceIPAndPORT;
    public String _comment;
    public OASTServerService _serverType;
    public boolean _isReaded = false;
    public String _color;

    public String description() {
        String description = " was received";
        if (!_sourceIPAndPORT.isEmpty())
            description +=  " from IP address <b>" + _sourceIPAndPORT + "</b>";
        if (!_timestamp.isEmpty())
            description += " at <b>" + _timestamp + "</b>";
        description += ".";
        return description;
    }

    static public Color getColor(int i, String color, boolean isSelected) throws NoSuchFieldException, IllegalAccessException {
        if (Objects.isNull(color)) {
            if (OASTFixed._api.userInterface().currentTheme().equals(Theme.DARK)) {
                if (isSelected)
                    return new Color(65, 80, 109);
                else if (i % 2 == 0)
                    return new Color(60,61,62);
                else
                    return new Color(73,74,75);
            } else {
                if (isSelected) {
                    //return new Color(255, 197, 153);
                    return new Color(202, 218, 240);
                } else if (i % 2 == 0)
                    return Color.WHITE;
                else
                    return new Color(242, 242, 242);
            }
        } else {
            if (isSelected)
                return ((Color) (Color.class.getField(color).get(null))).darker();
            return (Color) (Color.class.getField(color).get(null));
        }
    }

    public String getIpInfo() {
        String ip = IpInfoUtils.getIpFromIpAndPort(_sourceIPAndPORT);
        if (!ip.isEmpty() && OASTFixed.oastFixedTab.ipInfoMap.containsKey(ip)) {
            String ipinfo = OASTFixed.oastFixedTab.ipInfoMap.get(ip);
            return new JSONObject(ipinfo).toString(4);
        }
        return "";
    }
}
