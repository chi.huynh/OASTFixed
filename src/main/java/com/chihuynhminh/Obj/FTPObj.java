package com.chihuynhminh.Obj;

public class FTPObj extends OASTObj {
    private static final long serialVersionUID = -2084908406905931849L;
    public String _rawRequest;

    public String description() {
        String descriptionText = "";
        if (_serverType == OASTServerService.BurpCollab) {
            descriptionText += "The Collaborator server received ";
        } else {
            descriptionText += "The Interactsh server received ";
        }

        return descriptionText + "an <b>" + _protocol + "</b> connection for the domain name <b>"
                + _server + "</b><br><br>The connection" + super.description();
    }
}
