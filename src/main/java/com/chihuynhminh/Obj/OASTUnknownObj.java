package com.chihuynhminh.Obj;

public class OASTUnknownObj extends OASTObj {
    private static final long serialVersionUID = -4283248055605894864L;
    public String _data;

    @Override
    public String description() {
        String descriptionText = "";
        if (_serverType == OASTServerService.BurpCollab) {
            descriptionText += "The Collaborator server received ";
        } else {
            descriptionText += "The Interactsh server received ";
        }

        descriptionText += "an <b>" + _protocol + "</b> connection.";

        return descriptionText + "</b><br><br>The connection" + super.description();
    }
}
