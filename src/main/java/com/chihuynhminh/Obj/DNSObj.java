package com.chihuynhminh.Obj;

public class DNSObj extends OASTObj {
    public String _queryType;
    public byte[] _rawQuery;
    public String _rawRequest;
    public String _rawResponse;

    @Override
    public String description() {
        String descriptionText = "";
        if (_serverType == OASTServerService.BurpCollab) {
            descriptionText += "The Collaborator server received ";
        } else {
            descriptionText += "The Interactsh server received ";
        }

        descriptionText += "a <b>DNS</b> lookup of type <b>" + _queryType + "</b> for the domain name <b>";
        if (_payload.contains(_server))
            descriptionText += _payload;
        else
            descriptionText += _payload + "." + _server;

        return descriptionText + "</b><br><br>The query" + super.description();
    }
}
