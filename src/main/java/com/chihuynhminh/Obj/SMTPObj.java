package com.chihuynhminh.Obj;

public class SMTPObj extends OASTObj {
    public String _rawRequest;

    @Override
    public String description() {
        String descriptionText = "";
        if (_serverType == OASTServerService.BurpCollab) {
            descriptionText += "The Collaborator server received ";
        } else {
            descriptionText += "The Interactsh server received ";
        }

        descriptionText += "an <b>" + _protocol + "</b> connection for the domain name <b>";
        if (_payload.contains(_server))
            descriptionText += _payload;
        else
            descriptionText += _payload + "." + _server;

        return descriptionText + "</b><br><br>The connection" + super.description();
    }
}
