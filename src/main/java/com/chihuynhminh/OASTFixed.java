package com.chihuynhminh;

import burp.api.montoya.BurpExtension;
import burp.api.montoya.MontoyaApi;
import com.chihuynhminh.ui.OASTFixedTab;
import com.chihuynhminh.utils.DBUtils;
import com.chihuynhminh.utils.DNSUtils;

import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Properties;
import java.util.TimeZone;

public class OASTFixed implements BurpExtension {
    public static final String extension_name = "OAST Fixed";
    public static String extension_version = "";
    public static MontoyaApi _api;
    public static OASTFixedTab oastFixedTab;
    public static String validDomainNamePatten = "[^a-z0-9_-]";
    public static String default_config_folder = Paths.get(System.getProperty("user.home"), ".OASTFixed").toString();
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS'Z'");
    public static HashMap<Integer, String> qtype = DNSUtils.buildQTYPEMap();

    @Override
    public void initialize(MontoyaApi api) {
        _api = api;

        // Get version from pom file
        final Properties properties = new Properties();
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream("project.properties"));
            extension_version = properties.getProperty("version");
        } catch (IOException e) {
            e.printStackTrace(_api.logging().error());
        }

        // set extension name
        _api.extension().setName(extension_name + " " + extension_version);

        oastFixedTab = new OASTFixedTab();
        oastFixedTab.oastTable.setAutoCreateRowSorter(true);
        _api.userInterface().registerSuiteTab(extension_name, oastFixedTab.mainPanel);

        DBUtils.loadConfig();

        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
}
